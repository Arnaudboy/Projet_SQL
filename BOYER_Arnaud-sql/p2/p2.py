from faker import Faker
import sqlite3
import random
from pathlib import Path

faker = Faker()
con = sqlite3.connect(Path(Path(__file__).parent.parent, 'p1', 'p1.sql'))
cur = con.cursor()

food = ["", "salade César", "Poulet frites", "Steak haché frites", "Saumon en papillote", "12 Huîtres", "Cassoulet", "Tartiflette", "Raclette", "Hamburger frites", "Paëlla", "Pizza orientale", "Pizza reine", "Pizza margueritte", "Mousse au chocolat", "Tarte aux pommes", "Omelette", "Kebab", "Falafels", "Pudding", "Purée jambon"]

for i in range(1,21):
    cur.execute("INSERT INTO clients (firstName, lastName, address) values (?, ?, ?)", (faker.first_name(), faker.last_name(), faker.address()))
    cur.execute("INSERT INTO rooms (number, price) values (?, ?)", (i, 400))
    cur.execute("INSERT INTO restaurant (nameMeal, price) values (?, ?)", (food[i], random.randint(13, 19)))
    cur.execute("INSERT INTO bookings (roomNumber, booker, numberOfOccupants, checkInDate, checOutDate) values (?, ?, ?, ?, ?)", (i, i, random.randint(1, 4), faker.past_date(), faker.future_date()))

con.commit()
con.close()
